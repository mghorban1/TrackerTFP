To run the TMLR-HLS module in CMSSW:

Setting the CMSSW 11 environment:

    cmsrel CMSSW_11_1_0_pre1
    cd src/
    cmsenv

Git instructions:

    git cms-init
    git remote add maziar https://github.com/mzrghorbani/cmssw.git
    git checkout maziar

Adding CMSSW git packages:

    git cms-addpkg L1Trigger/TrackerDTC DataFormats/L1TrackTrigger SimTracker/TrackTriggerAssociation
    
Add modified TrackerTFP which contains Vivado HLS structure and algorithm:

    git clone https://gitlab.cern.ch/mghorban1/TrackerTFP.git L1Trigger/TrackerTFP

Compile and run:

    scram b -j4
    cmsRun L1Trigger/TrackerTFP/test/demo_cfg.py

Note: If the output text-files are required, change the directory location for dirIPBB in

    'L1Trigger/TrackerTFP/python/Demostrator_cfi.py'
